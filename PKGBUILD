# Maintainer: Oscar Campos <oscar.campos@thepimpam.com>
# Based on a previous work by:
#   Kenneth Henderick <kenneth@ketronic.be>

pkgname=zfs-snap-manager
pkgver=0.2.0
pkgrel=1
pkgdesc="A bunch of python2 scripts running as a service, using a configuration file to manage ZFS snapshots"
arch=('any')
url="https://github.com/khenderick/zfs-snap-manager"
license=('MIT')
depends=('zfs' 'python2>=2.7' 'openssh' 'mbuffer' 'python2-daemon16')
source=(
    "https://github.com/khenderick/$pkgname/archive/v$pkgver.zip"
    "zfs-snap-manager.initd"
    "zfssnapmanager.cfg"
)
md5sums=(
    '00b2f091a72b603513b5dcdf18222f5a'
    '56b58aa4822377be777899ef168a3a0c'
    'a8e397dc50c74663b88eece18435266a'
)

_int_initd() {
    install -Dm755 ${srcdir}/zfs-snap-manager.initd ${pkgdir}/etc/init.d/zfs-snap-manager
}

_int_etc() {
    install -D -m644 ${srcdir}/zfssnapmanager.cfg ${pkgdir}/etc/zfssnapmanager.cfg
}

package() {
    cd "$srcdir"/$pkgname-$pkgver
    mkdir -p "$pkgdir/usr/lib/zfs-snap-manager/"
    install -D -m644 "scripts/clean.py" "$pkgdir/usr/lib/zfs-snap-manager/clean.py"
    install -D -m644 "scripts/zfs.py" "$pkgdir/usr/lib/zfs-snap-manager/zfs.py"
    install -D -m644 "scripts/helper.py" "$pkgdir/usr/lib/zfs-snap-manager/helper.py"
    install -D -m755 "scripts/manager.py" "$pkgdir/usr/lib/zfs-snap-manager/manager.py"
    install -D -m644 "LICENSE" "$pkgdir/usr/share/licenses/$pkgname/LICENSE"

    mkdir -p "$pkgdir/usr/share/zfs-snap-manager/examples"
    install -D -m644 "examples/laptop.cfg" "$pkgdir/usr/share/zfs-snap-manager/examples/laptop.cfg"
    install -D -m644 "examples/nas.cfg" "$pkgdir/usr/share/zfs-snap-manager/examples/nas.cfg"
    install -D -m644 "examples/remotenas.cfg" "$pkgdir/usr/share/zfs-snap-manager/examples/remotenas.cfg"

    _int_initd ""

    _int_etc ""
}